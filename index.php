<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajax Multiple Image Upload with Edit Delete using PHP Mysql</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>
    <!-- Main Page Layout -->
    <br>
    <div class="container">
        <h3 style="text-align: center;">Ajax Multiple Image Upload with Edit Delete using PHP Mysql</h3>
        <br>
        <div align="right">
            <input type="file" id="multiple_files" name="multiple_files" multiple>
            <span class="text-muted">Only .jpg, png, .gif file allowed</span>
            <span id="error_multiple_files"></span>
        </div>
        <br>
        <br>
        <div class="table-responsive" id="image_table">


        </div>
    </div>

<!-- Edit Page Modal -->
<div id="imageModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" id="edit_image_form">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Image Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Image Name</label>
                        <input type="text" id="image_name" name="image_name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Image Description</label>
                        <input type="text" id="image_description" name="image_description" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="image_id" name="image_id" value="">
                    <input type="submit" name="submit" class="btn btn-info" value="Update">
                    <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

</body>

</html>

<script>
    
    $(document).ready(function() {
        load_image_data();
        // Fetch the Table ajax
        function load_image_data() {
            $.ajax({
                url: "fetch.php",
                method: "POST",
                success: function(data) {
                    $('#image_table').html(data);
                }
            });
        }
        // Multiple file upload function & upload ajax
        $('#multiple_files').change(function() {
            var error_images = '';
            var form_data = new FormData();
            var files = $('#multiple_files')[0].files;
            if (files.length > 10) {
                error_images += 'You can not select more than 10 files';
            } else {
                for (var i = 0; i < files.length; i++) {
                    var name = document.getElementById("multiple_files").files[i].name;
                    var ext = name.split('.').pop().toLowerCase();
                    // console.log(ext);
                    if (jQuery.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                        error_images += '<p>Invalid ' + i + ' File</p>';
                    }
                    var oFReader = new FileReader();
                    oFReader.readAsDataURL(document.getElementById("multiple_files").files[i]);
                    var f = document.getElementById("multiple_files").files[i];
                    var fsize = f.size || f.fileSize;
                    if (fsize > 2000000) {
                        error_images += '<p>' + i + 'File Size is very big</p>';
                    } else {
                        form_data.append("file[]", document.getElementById('multiple_files').files[i]);
                    }
                }
            }
            if (error_images == '') {
                $.ajax({
                    url: "upload.php",
                    method: "POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $('#error_multiple_files').html('<br /><label class="text-primary">Uploading...</label>');
                    },
                    success: function(data) {
                        $('#error_multiple_files').html('<br /><label class="text-success">Uploaded</label>')
                        load_image_data();
                    }
                });
            } else {
                $('#multiple_files').val('');
                $('#error_multiple_files').html("<span class='text-danger'>" + error_images + "</span>");
                return false;
            }
        });
        // Edit ajax
        $(document).on('click', '.edit', function() {
            var image_id = $(this).attr("id");
            $.ajax({
                url: "edit.php",
                method: "POST",
                data:{image_id:image_id},
                dataType: "JSON",
                success: function(data) {
                    $('#imageModal').modal('show');
                    $('#image_id').val(image_id);
                    $('#image_name').val(data.image_name);
                    $('#image_description').val(data.image_description);
                }
            });
        });
        // Delete ajax
        $(document).on('click','.delete',function() {
            var image_id = $(this).attr("id");
            var image_name = $(this).data("image_name");
            console.log(image_id);
            if(confirm("Are you sure you want to remove it ?")) {
                $.ajax({
                    url: "delete.php",
                    method: "POST",
                    data:{image_id:image_id, image_name:image_name},
                    success:function(data) {
                        load_image_data();
                        console.log("Image removed");
                    }
                });
            }
        });
        // Update ajax
        $('#edit_image_form').on('submit', function(event) {
            event.preventDefault();
            if($('#image_name').val()=='') {
                alert("Enter Image Name");
            } else {
                $.ajax({
                    url: "update.php",
                    method: "POST",
                    data:$('#edit_image_form').serialize(),
                    success:function(data) {
                        $('#imageModal').modal('hide');
                        load_image_data();
                        console.log('Image Details updated');
                    }
                });
            }
        });
    });
</script>